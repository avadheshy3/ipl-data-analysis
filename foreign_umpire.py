import csv
import matplotlib.pylab as plt
from umpire import umpire_country


def read_data():
    umpires = set()
    with open("matches.csv", "r") as csvfile:
        match_obj = csv.DictReader(csvfile, delimiter=",")
        for match in match_obj:
            umpires.add(match['umpire1'])
            umpires.add(match['umpire2'])
    country = set()
    umpire_with_country = {}
    for umpire in umpires:
        if(umpire_country.get(umpire)):
            foreign_country = umpire_country[umpire]
            if foreign_country not in country:
                umpire_with_country[foreign_country] = 1
                country.add(foreign_country)
            else:
                umpire_with_country[foreign_country] += 1

    return umpire_with_country


def display(country, umpire):
    plt.bar(country, umpire)
    plt.xlabel('country name')
    plt.ylabel('number of umpire')
    plt.show()


if __name__ == '__main__':
    counter_count_umpire = read_data()
    display(counter_count_umpire.keys(), counter_count_umpire.values())
