import csv
import matplotlib.pyplot as plt


def read_data():
    match_ids = []
    extra_runs_per_teams = {}

    with open('matches.csv') as match:

        match_obj = csv.DictReader(match)

        for match in match_obj:
            if match['season'] == '2016':
                match_ids.append(match['id'])

    with open('deliveries.csv') as deliveries:
        delivery_obj = csv.DictReader(deliveries)
        for delivery in delivery_obj:
            if delivery['match_id'] in match_ids:
                team = delivery['bowling_team']
                extras = int(delivery['extra_runs'])
                if team in extra_runs_per_teams.keys():
                    extra_runs_per_teams[team] += extras
                else:
                    extra_runs_per_teams[team] = extras
    return extra_runs_per_teams


def display(teams, extras):
    plt.bar(teams, extras)
    plt.xlabel('names of the team')
    plt.ylabel('total extra run leaked by each team in year 2016')
    plt.xticks(rotation=30, ha="right")
    plt.show()


if __name__ == '__main__':
    extra_runs = read_data()
    display(extra_runs.keys(), extra_runs.values())
