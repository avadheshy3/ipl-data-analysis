import csv
import matplotlib.pyplot as plt


def read_data():
    team_data = {}
    teams = set()
    win = [[0 for i in range(13)] for j in range(10)]
    with open("matches.csv", "r") as match:
        match_obj = csv.DictReader(match)
        for row in match_obj:
            year = row['season']
            winer = row['winner']
            teams.add(row['team1'])
            if winer is not None:
                if year in team_data.keys():
                    if winer in team_data[year].keys():
                        team_data[year][winer] += 1
                    else:
                        team_data[year][winer] = 1
                else:
                    team_data[year] = {winer: 1}
    teams = list(sorted(teams))
    year = []
    team_winnings_by_year = []
    for i in sorted(team_data.keys()):
        year.append(i)
        win = []
        for j in teams:
            if j in team_data[i].keys():
                win.append(team_data[i][j])
            else:
                win.append(0)
        team_winnings_by_year.append(win)
    return [teams, team_winnings_by_year]


def display(teams, winings):
    years = [year for year in range(2008, 2018)]
    temp = [0 for i in range(len(teams))]
    for i in range(len(winings)):
        plt.bar(teams, winings[i], bottom=temp)
        for j in range(len(teams)):
            temp[j] += winings[i][j]
    plt.legend(years)
    plt.xticks(rotation=30, ha="right")
    plt.show()


if __name__ == '__main__':
    win = read_data()
    display(win[0], win[1])
