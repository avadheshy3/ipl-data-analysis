import csv
import matplotlib.pylab as plt
# this dictionary will store the name with their total runs for rcb team
batsman = {}


def read_data():
    with open("deliveries.csv", "r") as csvfile:
        delivery_obj = csv.DictReader(csvfile)
        for delivery in delivery_obj:
            if delivery['batting_team'] == 'Royal Challengers Bangalore':
                batter = delivery['batsman']
                run = int(delivery['batsman_runs'])
                if batter in batsman.keys():
                    batsman[batter] += run
                else:
                    batsman[batter] = run


def topBatsMan(batsman):
    """This function will collect the information of
    the rcb batsman who have scored more
    than hundred run"""
    batsman = dict(sorted(batsman.items(), key=lambda x: x[1], reverse=True))
    players = []
    runs = []
    for i in batsman.keys():
        players.append(i)
        runs.append(batsman[i])
    return [players, runs]


def display(batsman, run):
    """This function will display top batsman of rcb team who have scored more than
    hundred runs for rcb"""
    plt.bar(batsman, run)
    plt.ylabel('runs score by top 10 batsman of rcb team')
    plt.xlabel('name of the tob batsman for rcb')
    plt.show()


if __name__ == '__main__':
    read_data()
    topbatsman = topBatsMan(batsman)
    display(topbatsman[0][:10], topbatsman[1][:10])
    # this has been completed
