import csv
import matplotlib.pyplot as plt


def read_data():
    """In this function data are read and store in bowler_data as dictionary"""
    bowlers_data = {}
    match_ids = []
    with open('matches.csv') as match:

        match_obj = csv.DictReader(match)

        for maches in match_obj:
            if maches['season'] == '2015':
                match_ids.append(maches['id'])
    with open('deliveries.csv') as deliveries:
        deliveriy_obj = csv.DictReader(deliveries)
        for delivery in deliveriy_obj:

            if delivery['match_id'] in match_ids:
                bowler = delivery['bowler']
                run = int(delivery['total_runs'])
                extra_runs = int(delivery['wide_runs'])
                extra_runs += int(delivery['noball_runs'])
                if bowler in bowlers_data.keys():
                    if extra_runs == 0:
                        bowlers_data[bowler][0] += 1
                    bowlers_data[bowler][1] += run
                else:
                    bowlers_data[bowler] = [0, 0]
                    if extra_runs == 0:
                        bowlers_data[bowler][0] = 1
                    else:
                        bowlers_data[bowler][1] = run
    return bowlers_data


def economy_calc(a):
    """ This function is used to calculate economy
    of each bowler"""
    bowler = []
    economy = []
    for i in a.keys():
        bowler.append(i)
        economy.append(round((6*a[i][1])/a[i][0], 1))
    for i in range(len(bowler)):
        for j in range(i+1, len(bowler)):
            if economy[i] > economy[j]:
                economy[i], economy[j] = economy[j], economy[i]
                bowler[i], bowler[j] = bowler[j], bowler[i]
    return [bowler, economy]


def display(bowler, economy):
    """ this function is used to display top ten
    economical bowler """
    plt.bar(bowler[:10], economy[:10])
    plt.xlabel('names of the bowler')
    plt.ylabel('Top ten economical bowler in season 2015')
    plt.show()


if __name__ == '__main__':
    bowlers_data = read_data()
    bowlers_economy = economy_calc(bowlers_data)
    display(bowlers_economy[0], bowlers_economy[1])
