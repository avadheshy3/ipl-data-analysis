import csv
import matplotlib.pylab as plt
seasons = [season for season in range(2008, 2018)]


def data_read():
    matches_played = []
    for year in range(len(seasons)):
        season = []
        season.append(year)
        season.append({})
        matches_played.append(season)
    team_index = 1
    with open("matches.csv", "r") as csvfile:
        match_obj = csv.DictReader(csvfile, delimiter=",")
        for match in match_obj:
            season = int(match['season']) - 2008
            team1, team2 = match['team1'], match['team2']
            if team1 in matches_played[season][team_index].keys():
                matches_played[season][team_index][team1] += 1
            else:
                matches_played[season][team_index][team1] = 1
            if team2 in matches_played[season][team_index].keys():
                matches_played[season][team_index][team2] += 1
            else:
                matches_played[season][team_index][team2] = 1
    teams = []
    for season in range(len(matches_played)):
        teams.extend(matches_played[season][team_index].keys())
    teams = list(sorted(set(teams)))

    match_played = []
    # making a list of match played by all the teams in  a year
    for season in range(len(seasons)):
        played = []
        for team in range(len(teams)):
            if teams[team] in matches_played[season][1].keys():
                played.append(matches_played[season][1][teams[team]])
            else:
                played.append(0)
        match_played.append(played)

    team_matches_with_year = [[0 for team in range(13)]for year in range(10)]
    for season in range(10):
        for team in range(13):
            team_matches_with_year[season][team] = match_played[season][team]
    return [teams, team_matches_with_year]


def display(teams_1, arr):
    fig, ax = plt.subplots()
    a = [0 for team in range(len(teams_1))]
    for i in range(len(arr)):
        plt.bar(teams_1, arr[i], bottom=a)
        for j in range(13):
            a[j] += arr[i][j]
    ax.set_ylabel('Number of matches')
    ax.set_title('IPL Teams')
    plt.legend(seasons)
    plt.xticks(rotation=30, ha='right')
    plt.show()


if __name__ == '__main__':
    data = data_read()
    display(data[0], data[1])
