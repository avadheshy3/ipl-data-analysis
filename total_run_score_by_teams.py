import csv
import matplotlib.pylab as plt


def read_data():
    runs_by_team = {}
    with open("deliveries.csv", "r") as csvfile:
        reader_variable = csv.DictReader(csvfile, delimiter=",")
        for row in reader_variable:
            team = row['batting_team']
            run = int(row['total_runs'])
            if team in runs_by_team.keys():
                runs_by_team[team] += run
            else:
                runs_by_team[team] = run
    return runs_by_team


def display(teams, runs):
    """this will show the runs made by each team"""
    plt.bar(teams, runs)
    plt.xlabel('run made by each team')
    plt.ylabel('name of each teams')
    plt.xticks(rotation=30, ha='right')
    plt.show()


if __name__ == '__main__':
    team = read_data()
    display(team.keys(), team.values())
