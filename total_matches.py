import csv
import matplotlib.pyplot as plt
# making a dictionay which can store total matche played year wise


def read_data():
    match_per_year = {}
    with open('matches.csv') as f:
        match_obj = csv.DictReader(f)
        for match in match_obj:
            year = match['season']
            if year in match_per_year.keys():
                match_per_year[year] += 1
            else:
                match_per_year[year] = 1
    return match_per_year


def display(season, match):
    plt.bar(season, match)
    plt.legend()
    plt.xlabel('year')
    plt.ylabel('total matches played per year')
    plt.show()


if __name__ == '__main__':
    match = read_data()
    display(match.keys(), match.values())
    # this is also completed
